#include <stdio.h>
#include <stdlib.h>

#include "tnl_bddcml_interface.h"

void setupMatrix( int m, int n, double *hostMatrix)
{
   // Set-up host matrix and transposed matrix which will be transferred on the GPU
   for( int col = 0; col < n; col++ ) {
      for( int row = 0; row < m; row++ ) {
         hostMatrix[col*m+row] = col;
      }
   }
}

void setupVector( int m, double *hostVector)
{
   // Set-up host matrix and transposed matrix which will be transferred on the GPU
   for( int row = 0; row < m; row++ ) {
      hostVector[row] = 1.;
   }
}

void printVector( int m, double *hostVector)
{
   // Set-up host matrix and transposed matrix which will be transferred on the GPU
   printf("vector = [ \n");
   for( int row = 0; row < m; row++ ) {
      printf("%lf \n",hostVector[row]);
   }
   printf("]\n");
}

int main( int argc, char* argv[] )
{
   // test repeated y = A*x
   const int m = 20;
   const int n = 10;
   const int matrixIterations = 2;

   void *deviceMatrix;
   double *hostMatrix;
   double *x, *y;

   hostMatrix = malloc(m*n*sizeof(double));

   setupMatrix(m, n, hostMatrix);

   x = malloc(n*sizeof(double));
   y = malloc(m*sizeof(double));

   // Sequential test
   tnl_create_and_set_matrix_on_gpu(m, n, hostMatrix, m, &deviceMatrix);

   for (int iter = 0; iter < matrixIterations; iter++) {
      setupVector(n, x);
      tnl_gemv_matrix_on_gpu(m, n, &deviceMatrix, x, y);
      printVector(m, y);
   }

   tnl_clear_matrix_on_gpu(&deviceMatrix);

   free(x);
   free(y);
   free(hostMatrix);
}
