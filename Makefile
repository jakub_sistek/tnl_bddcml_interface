# Uncomment the following line to enable CUDA
WITH_CUDA = yes

TNL_DIR = ${HOME}/programming/tnl-dev/src
MPARK_VARIANT_DIR = ${HOME}/software/mpark/variant/include

TARGET = test_tnl_bddcml_interface test_tnl_bddcml_interface_fortran
LIBTNL_BDDCML_INTERFACE = libtnl_bddcml_interface.a

LDFLAGS = -L/usr/local/cuda/lib64 -lcudart
LDFLAGS += -L. -ltnl_bddcml_interface
LDFLAGS += -L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core

ifdef WITH_CUDA
   CXX = nvcc
   CXX_FLAGS = -ccbin g++-9 -I${TNL_DIR} -DHAVE_CUDA -g --std c++14 --expt-extended-lambda --expt-relaxed-constexpr -I${MPARK_VARIANT_DIR} -gencode arch=compute_50,code=sm_50
   CC = gcc-9
   FC = gfortran
else 
   CXX = mpic++
   CXX_FLAGS = -I${TNL_DIR} -I${MKLROOT}/include -std=c++14
   CC = gcc-9
   FC = gfortran
endif     

SOURCES = test_tnl_bddcml_interface.c tnl_bddcml_interface_mod.f90 test_tnl_bddcml_interface_fortran.f90
HEADERS = tnl_bddcml_interface.hpp tnl_bddcml_interface.h
OBJECTS = test_tnl_bddcml_interface.o tnl_bddcml_interface_mod.o test_tnl_bddcml_interface_fortran.o
CUDA_SOURCES = tnl_bddcml_interface.cu
DIST = $(SOURCES) $(CUDA_SOURCES) $(HEADERS) Makefile

ifdef WITH_CUDA
   OBJECTS = tnl_bddcml_interface.o
endif

all: $(TARGET)

test_tnl_bddcml_interface.o: tnl_bddcml_interface.o

tnl_bddcml_interface.o: tnl_bddcml_interface.cu tnl_bddcml_interface.hpp

clean:
	rm -f *.o *.mod

cleanall: clean
	rm -f $(TARGET) $(LIBTNL_BDDCML_INTERFACE)

dist: $(DIST)
	tar zcvf $(TARGET).tgz $(DIST)

$(LIBTNL_BDDCML_INTERFACE): tnl_bddcml_interface_mod.o tnl_bddcml_interface.o
	ar cvr $@ $^
	ranlib $@

test_tnl_bddcml_interface_fortran: test_tnl_bddcml_interface_fortran.o $(LIBTNL_BDDCML_INTERFACE) 
	$(FC) -o $@ $^ $(LDFLAGS) -lstdc++

test_tnl_bddcml_interface: test_tnl_bddcml_interface.o $(LIBTNL_BDDCML_INTERFACE)
	$(CXX) -o $@ $^ $(LDFLAGS)

test_tnl_bddcml_interface_fortran.o: $(LIBTNL_BDDCML_INTERFACE)

tnl_bddcml_interface_mod.o: tnl_bddcml_interface_mod.f90

tnl_bddcml_interface.o: tnl_bddcml_interface.hpp


%.o: %.f90
	$(FC) $(FCFLAGS) -c -o $@ $<

%.o: %.cu
	$(CXX) $(CPPFLAGS) $(CXX_FLAGS) -c -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
