#include <assert.h>

#include <TNL/Matrices/DenseMatrix.h>

#define HAVE_CUDA

typedef TNL::Matrices::DenseMatrixView<
   double, TNL::Devices::Host, int, TNL::Algorithms::Segments::ColumnMajorOrder > MatrixHostView;
typedef TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, int >  MatrixGPU;

extern "C"
void tnl_create_and_set_matrix_on_gpu( int m, int n, double *A, int lda, void **dA)
{
   TNL::Containers::VectorView< double > host_matrix_values;
   host_matrix_values.bind(A, m*n);

   MatrixHostView host_matrix_view(m, n, host_matrix_values);

   MatrixGPU *matrixDevice = new MatrixGPU();
   matrixDevice->setDimensions(n, m);

   try {
      *matrixDevice = host_matrix_view;
   } catch( ... ) {};

   *dA = static_cast<void *>(matrixDevice);
}

extern "C"
void tnl_clear_matrix_on_gpu(void **dA)
{
   MatrixGPU *matrixDevice = static_cast<MatrixGPU *>(*dA);

   delete matrixDevice;
}

extern "C"
void tnl_gemv_matrix_on_gpu(int m, int n, void **dA, double *x, double *y)
{
   MatrixGPU *matrixDevice = static_cast<MatrixGPU *>(*dA);

   typedef TNL::Containers::VectorView< double, TNL::Devices::Host, int > VectorHostView;
   typedef TNL::Containers::Vector< double, TNL::Devices::Cuda, int >     VectorGPU;

   assert(m == matrixDevice->getRows());
   assert(n == matrixDevice->getColumns());

   VectorHostView x_host(x, n);
   VectorHostView y_host(y, m);
   VectorGPU dx(n), dy(m);

   try {
      dx = x_host; // copy vector to GPU
      dy = 0.;
      matrixDevice->vectorProduct(dx, dy);
      y_host = dy; // copy vector from GPU
   } catch( ... ) { std::cout << "Multiplication on GPU not succesfull." << std::endl;};
}
