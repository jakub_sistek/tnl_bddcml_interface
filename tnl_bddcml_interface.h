void tnl_create_and_set_matrix_on_gpu( int m, int n, double *A, int lda, void **dA);

void tnl_clear_matrix_on_gpu(void **dA);

void tnl_gemv_matrix_on_gpu(int m, int n, void **dA, double *x, double *y);