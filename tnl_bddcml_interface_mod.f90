module tnl_bddcml_interface
    use iso_c_binding
    implicit none

    ! Interfaces of the C functions.
    interface
        subroutine tnl_create_and_set_matrix_on_gpu_c(m, n, A, lda, dA) &
          bind(c, name='tnl_create_and_set_matrix_on_gpu')
            use iso_c_binding
            implicit none
            integer(kind=c_int), value :: m
            integer(kind=c_int), value :: n
            type(c_ptr), value :: A
            integer(kind=c_int), value :: lda
            integer(kind=8) :: dA
        end subroutine
    end interface

    interface
        subroutine tnl_clear_matrix_on_gpu_c(dA) &
          bind(c, name='tnl_clear_matrix_on_gpu')
            use iso_c_binding
            implicit none
            integer(kind=8) :: dA
        end subroutine
    end interface

    interface
        subroutine tnl_gemv_matrix_on_gpu_c(m, n, dA, x, y) &
          bind(c, name='tnl_gemv_matrix_on_gpu')
            use iso_c_binding
            implicit none
            integer(kind=c_int), value :: m
            integer(kind=c_int), value :: n
            integer(kind=8) :: dA
            type(c_ptr), value :: x
            type(c_ptr), value :: y
        end subroutine
    end interface

    contains

    ! Wrappers of the C functions.
    subroutine tnl_create_and_set_matrix_on_gpu(m, n, A, lda, dA)
        use iso_c_binding
        implicit none
        integer(kind=c_int) :: m
        integer(kind=c_int) :: n
        real(kind=c_double), target :: A(lda,n)
        integer(kind=c_int) :: lda
        integer(kind=8) :: dA

        call tnl_create_and_set_matrix_on_gpu_c(m, n, c_loc(A), lda, dA)
    end subroutine

    subroutine tnl_clear_matrix_on_gpu(dA)
        use iso_c_binding
        implicit none
        integer(kind=8) :: dA

        call tnl_clear_matrix_on_gpu_c(dA)
    end subroutine

    subroutine tnl_gemv_matrix_on_gpu(m, n, dA, x, y)
        use iso_c_binding
        implicit none
        integer(kind=c_int) :: m
        integer(kind=c_int) :: n
        integer(kind=8) :: dA
        real(kind=c_double), target :: x(n)
        real(kind=c_double), target :: y(m)

        call tnl_gemv_matrix_on_gpu_c(m, n, dA, c_loc(x), c_loc(y))
    end subroutine

end module tnl_bddcml_interface
