program test_tnl_bddcml_interface_fortran
    use, intrinsic :: iso_fortran_env
    use iso_c_binding

    use tnl_bddcml_interface
    implicit none

    integer, parameter :: dp = c_double

    ! set working precision, this value is rewritten for different precisions
    integer, parameter :: wp = dp

    ! matrices
    real(wp), allocatable :: A(:,:)
    real(wp), allocatable :: x(:)
    real(wp), allocatable :: y(:)
    real(wp), allocatable :: yref(:)
    real(wp), allocatable :: diff(:)

    ! pointer to the array
    integer(kind=8),target :: dA

    ! dimensions
    integer :: m
    integer :: n
    integer :: lda
    integer :: i, j

    integer :: iter, niter 
    real(wp) :: norm

    ! Set parameters.
    m    = 4000 ! number of rows
    n    = 3000 ! number of columns
    niter = 2 ! number of iterations

    ! Initialize arrays.
    lda  = m    ! leading dimension of A
    allocate(A(lda, n))
    do j = 1,n
       A(:,j) = j
    end do

    ! initialize vectors
    allocate(x(n), y(m), diff(m))
    allocate(yref(m))

    write(*,'(a, i6, a, i6)') " Testing multiplication with a matrix ", m, " x ", n
    write(*,'(a, i6, a)')     " and ", niter, " iterations."

    !==============================================
    ! Copy matrix to GPU.
    !==============================================
    call tnl_create_and_set_matrix_on_gpu(m, n, A, lda, dA)

    !==============================================
    ! Y = A * X
    !==============================================
    do iter = 1,niter
       x = 1.
       call tnl_gemv_matrix_on_gpu(m, n, dA, x, y)
       yref = matmul(A,x)
       diff = y - yref
       norm = sqrt(dot_product(diff,diff))
       write(*,'(a, f6.3)')     " Norm of the difference vector is ", norm
    end do

    !==============================================
    ! Deallocate the matrix on GPU.
    !==============================================
    call tnl_clear_matrix_on_gpu(dA)

    deallocate(A)
    deallocate(x)
    deallocate(y)

end program
